package com.example.cdt.process;

import org.springframework.batch.item.ItemProcessor;

import com.example.cdt.model.Coesui;

public class TransformProcessor implements ItemProcessor<Coesui,Coesui> {
	
	public Coesui process(Coesui coesui) throws Exception
    {
		
        if (coesui.getMetricvalue() == null){

        	return null;
        }
         
        try
        {
        	// metric value * 100
        	double value = Double.valueOf(coesui.getMetricvalue());
        	String newValue = ""+(value*100);
        	
        	
        	//date format
        	String str = coesui.getDateTime();
        	System.out.println(str);
        	
        	coesui.setMetricvalue(newValue);
        }
        catch (NumberFormatException e) {
            System.out.println("Invalid Coesui id : " + coesui.getMetricvalue());
            return null;
        }
        return coesui;
    }

}
